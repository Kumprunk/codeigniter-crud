<!DOCTYPE html>
<html>
<head>
	<?php

		$this->load->view('admin/_template/head.php')

	?>
</head>

<body id="page-top">
	<?php

		$this->load->view('admin/_template/navbar.php')

	?>
	<div id="wrapper">
		
		<?php

			$this->load->view('admin/_template/sidebar.php')

		?>
		<div id="content-wrapper">
			
			<div class="container-fluid">
				
				<?php

					$this->load->view('admin/_template/breadcumb.php')

				?>

				<?php

					if ($this->session->flashdata('success')): 
						
				?>

				<div class="alert alert-success" role="alert">
					<?php

						print $this->session->flashdata('success')

					?>
				</div>
					<?php endif; ?>

				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php print site_url('admin/products/') ?>"><i class="fas fa-arrow-left"></i>Back</a>
					</div>
					<div class="card-body">
						
						<form action="<?php base_url('admin/product/add') ?>" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<label for="name">Name*</label>
								<input class="form-control <?php print form_error('name') ? 'is-invalid':'' ?>" type="text" name="name" placeholder="Product Name"/>
								<div class="invalid-feedback">
									<?php print form_error('name') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="name">Price*</label>
								<input class="form-control <?php print form_error('price') ? 'is-invalid':'' ?>" type="number" name="price" min="0" placeholder="Product Price"/>
								<div class="invalid-feedback">
									<?php print form_error('price') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="name">Photo*</label>
								<input class="form-control-file <?php echo form_error('price') ? 'is-invalid':'' ?>"
								 type="file" name="image" />
								<div class="invalid-feedback">
									<?php echo form_error('image') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="name">Description*</label>
								<textarea class="form-control <?php echo form_error('description') ? 'is-invalid':'' ?>"
								 name="description" placeholder="Product description..."></textarea>
								<div class="invalid-feedback">
									<?php echo form_error('description') ?>
								</div>
							</div>

							<input class="btn btn-success" type="submit" name="btn" value="Save" />
						</form>
					</div>
					<div class="card-footer small text-muted">
							
					</div>
						
				</div>
					<?php 

							$this->load->view('admin/_template/footer.php')

					?>
			</div>
		</div>
			<?php 

					$this->load->view('admin/_template/scrolltop.php')

			?>
	</div>

</body>
</html>